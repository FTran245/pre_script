#include <iostream>
#include <string>
#include <iterator>
#include <functional>
#include <vector>
#include <algorithm>
#include <stdio.h>
#include <fstream>
#include "Pre_script.h"
#include "misc.h"
#include "tinyxml2-master\tinyxml2-master\tinyxml2.h"
#include <opencv2\opencv.hpp>
#include "opencv2\imgproc\imgproc.hpp"
#include "opencv2\core\core.hpp"
#include "opencv2\highgui\highgui.hpp"

using namespace std;
using namespace cv;
using namespace tinyxml2;


vector<double> readLUT(string fn_ResParam)
{
	// https://stackoverflow.com/questions/15138353/how-to-read-a-binary-file-into-a-vector-of-unsigned-chars
	// https://stackoverflow.com/questions/41754230/how-to-read-double-from-binary-file-and-save-it-in-a-vector
	// https://codereview.stackexchange.com/questions/75147/reading-binary-file-of-doubles

	// The LUT will go in rescaleParam
	// We need rescaleParamtemp as a pointer to read in the LUT
	// Make sure to initialize pointer to NULL because C++ won't let you operate on it if you assign it in an if()
	ifstream fid_rescaleParam(fn_ResParam, ios::in | std::ios::binary);
	vector<double> rescaleParam;
	vector<double> *rescaleParamtemp = NULL;

	// Check if valid file
	if (fid_rescaleParam)
	{
		streampos fileSize;
		double *fileBuffer;
		size_t sizeOfBuffer;

		// get the size of the file
		fid_rescaleParam.seekg(0, ios::end);
		fileSize = fid_rescaleParam.tellg();
		fid_rescaleParam.seekg(0, ios::beg);

		sizeOfBuffer = fileSize / sizeof(double);
		fileBuffer = new double[sizeOfBuffer];

		fid_rescaleParam.read(reinterpret_cast<char*>(fileBuffer), fileSize);

		// Convert double array to vector
		rescaleParamtemp = new vector<double>(fileBuffer, fileBuffer + sizeOfBuffer);

		free(fileBuffer);
	}

	// Reassign LUT, make sure to add 1 to the whole thing. That's what the pipeline has.
	for (int ii = 0; ii < rescaleParamtemp->size(); ii++)
	{
		rescaleParam.push_back(rescaleParamtemp->at(ii) + 1);
	}

	// Delete the pointer and return
	delete rescaleParamtemp;
	return rescaleParam;
}

// Translated from MATLAB
Mat hilbert(Mat rawData)
{
	Mat fftData;
	int n = rawData.rows;

	//dftExample();

	// n point DFT (fft) over columns
	// https://docs.opencv.org/2.4/modules/core/doc/operations_on_arrays.html#dft
	//imshow("rawData", rawData);
	//waitKey();
	fftData = fft1D(rawData);
	//checkDFT(fftData);

	//test inverse transform here
	//Mat testing;
	//dft(fftData, testing, DFT_INVERSE | DFT_REAL_OUTPUT);
	//checkDFT(testing);

	vector<float> h(n, 0); // initialize vector of zeros based on size

	// Even and nonempty
	if (n > 0 && 2 * trunc(n / 2) == n) // trunc will truncate decimal values (we need it to round towards 0)
	{
		// Watch out for indices, MATLAB starts at 1, C/C++ starts at 0
		/* MATLAB version 
		h([n/2 + 1]) = 1; */
		h[0] = 1;
		h[(n/2) + 1 - 1] = 1;

		/* MATLAB version 
		h(2:(n+1/)2) = 2; */ 
		for (int ind = 2 - 1; ind <= n/2 - 1; ind++)
		{
			h[ind] = 2;
		}
	}
	// Odd and nonempty
	else if (n > 0)
	{
		h[0] = 1;
		for (int ind = 2 - 1; ind <= (n + 1)/2 - 1; ind++)
		{
			h[ind] = 2;
		}
	}

	vector<vector<float>> h2(400, vector<float>(1024));
	//h2.resize(1024);
	for (int jj = 0; jj < h2.size(); jj++)
	{
		h2[jj] = h;
	}

	Mat hMatrix(1024, 400, CV_32FC1);
	for (int col = 0; col < h2.size(); col++)
	{
		for (int ro = 0; ro < h2[0].size(); ro++)
		{
			hMatrix.at<float>(ro, col) = h2.at(col).at(ro);
		}
	}

	// Do ifft here
	/* MATLAB version 
	x = ifft(x.*h(:,ones(1,size(x,2))),[],1); 
	will return 1024x400 */

	/* Element-wise multiplication is done like this: output = A.mul(B) for Mat objects */
	Mat hilbertData(1024, 400, CV_32F, Scalar(0, 0));

	Mat planes[] = { Mat_<float>(hilbertData), Mat::zeros(hilbertData.size(), CV_32F) };

	// split into real/imag parts and do the matrix multiplication on each
	// planes[0] = real
	// planes[1] = imag
	split(fftData, planes);
	planes[0] = hMatrix.mul(planes[0]);
	planes[1] = hMatrix.mul(planes[1]);

	// merge the complex data into hilbertData
	// this combines 2 channel Mat object into 1
	merge(planes, 2, hilbertData);

	// https://stackoverflow.com/questions/10269456/inverse-fourier-transformation-in-opencv
	// IFFT
	Mat inverseTransform;
	merge(planes, 2, inverseTransform);

	cout << "Inverse transform...\n";
	dft(hilbertData, inverseTransform, DFT_ROWS | DFT_INVERSE | DFT_REAL_OUTPUT);
	cout << "Inverse: " << inverseTransform.channels() << endl;
	cout << "Hilbert: " << hilbertData.channels() << endl;
	//checkDFT(inverseTransform);

 	return inverseTransform;
}



// Load the parameters from the .xml file into parameters class
void getParameters(volume_parameters &parameters, XMLDocument &doc)
{
	//XMLText *widthNode = doc.FirstChildElement("MonsterList")->GetText();
	// const char *test = doc.FirstChildElement("MonsterList")->FirstChildElement("Monster")->FirstChildElement("Volume_Size")->Attribute("Width");
	// const char *test22 = doc.FirstChildElement("MonsterList")->FirstChildElement("Monster")->FirstChildElement("Volume_Size")->Attribute("Height");

	parameters.width = doc.FirstChildElement("MonsterList")->FirstChildElement("Monster")->FirstChildElement("Volume_Size")->IntAttribute("Width");
	parameters.height = doc.FirstChildElement("MonsterList")->FirstChildElement("Monster")->FirstChildElement("Volume_Size")->IntAttribute("Height");
	parameters.Number_of_Frames = doc.FirstChildElement("MonsterList")->FirstChildElement("Monster")->FirstChildElement("Volume_Size")->IntAttribute("Number_of_Frames");
	parameters.Number_of_Volumes = doc.FirstChildElement("MonsterList")->FirstChildElement("Monster")->FirstChildElement("Volume_Size")->IntAttribute("Number_of_Volumes");

	parameters.ImageSize = doc.FirstChildElement("MonsterList")->FirstChildElement("Monster")->FirstChildElement("Scanning_Parameters")->IntAttribute("X_Scan_Range");
	parameters.Number_of_BM_scans = doc.FirstChildElement("MonsterList")->FirstChildElement("Monster")->FirstChildElement("Scanning_Parameters")->IntAttribute("X_Scan_Range");
}

// The AO processing and dispersion compensation. This will be very difficult
Mat proCplxAOOCT(volume_parameters &parameters, const char *fn_unp, vector<double> &LUT)
{
	Mat rawData(parameters.width, parameters.height, CV_32FC1);
	long lSize;

	float *buffer;
	size_t result;
	int ref_Frame = 200;

	// Open the file in "read" mode, rb means read+binary file
	FILE *pFile;
	pFile = fopen(fn_unp, "rb");
	if (pFile == NULL)
	{
		cout << "Unable to open file " << endl;
	}

	// fseek( FILE * stream, long int offset, int origin);
	/* sets the pointer of the file to offset with respect to origin, in this case we're setting the pointer
	to the SEEK_END of th file with 0 offset - literally setting the pointer to the end */
	//fseek(pFile, 2*parameters.width*parameters.height*(ref_Frame-1), SEEK_SET);
	fseek(pFile, 0, SEEK_END); 
	lSize = ftell(pFile); // get the current position of the pointer of file (number of bytes)Kagdudeguy

	rewind(pFile); // reset the pointer to the beginning of the file

	cout << "Total file size bytes: " << lSize << endl;

	// Pretty sure this is the volume now
	// Fai used the same code, except maybe not a char casted memory allocation
	//buffer = (float*)malloc(lSize);
	buffer = new float[lSize]; // use new/delete instead of malloc/free because C++ and it's faster, allocate a chunk of memory size lSize
	 
	/* size_t fread ( void * ptr, size_t size, size_t count, FILE * stream );
	Reads an array of count elements, each one with a size of size bytes, from the stream and stores them in the 
	block of memory specified by ptr. The position indicator of the stream is advanced by the total amount of bytes read.
	The total amount of bytes read if successful is (size*count). */
	result = fread(buffer, sizeof(float), lSize / sizeof(float), pFile);
	cout << "Number of elements of size float: " << lSize / sizeof(float) << endl;
	/*if (results != lSize / sizeof(float)) 
	{ 
		fputs("Reading error", stderr); exit(3); 
	}*/

	memcpy(rawData.data, buffer + 1*parameters.width*parameters.height, sizeof(float)*(parameters.height*parameters.width));

	Mat pl;
	normalize(rawData, pl, 0, 1, CV_MINMAX); //this is basically mat2gray()
	//imshow("Display window", pl); // Show our image inside it.
	//waitKey();

	//dftExample();

	// Hilbert transform then FFT
	// Perhaps we can convert MATLAB's hilbert() to C/C++ using MATLAB Coder
	// later change to pass in by reference to increase run time
	Mat hilbertData;
	Mat ref_FFTData(parameters.width, parameters.height, CV_32FC1);
	hilbertData = hilbert(rawData);

	// FFT on the hilbert data
	dft(hilbertData, ref_FFTData);

	// ref_Aline = ref_FFTData(:, end/2); - for MATLAB
	//vector<float> ref_Aline;
	Mat ref_Aline(1, 1024, CV_32FC1); //1 column, 1024 rows
	ref_Aline = ref_FFTData.col(ref_FFTData.cols/2);

	int offSetIdx = 15;

	// diffPhases = estPhaseDiff(ref_Aline, ref_FFTData, offsetIdx);
	/* diffPhases is a 1x400 column vector */
	vector<double> place;
	place = estPhaseDiff(ref_Aline, ref_FFTData, offSetIdx);

	//free(buffer); // do this at the end
	delete buffer;
	return rawData; // you will end up with a complex double matrix
}


// Estimate different phases
vector<double> estPhaseDiff(Mat ref_Aline, Mat ref_FFTData, int offsetIdx)
{
	vector<double> DiffPhase;
	Mat ref_fftData_R_I[] = { Mat_<float>(ref_FFTData), Mat::zeros(ref_FFTData.size(), CV_32F) }; // create 2 planes, real and complex
	Mat ref_Aline_R_I[] = { Mat_<float>(ref_Aline), Mat::zeros(ref_Aline.size(), CV_32F) };

	/* ref_fftData_R   = padarray(real(refAline), [(2^15 - size(refAline,1)) 0], 'post'); */
	// split ref_Aline into real and imag parts
	split(ref_Aline, ref_Aline_R_I);

	// zero pad the real and imag part of ref_Aline
	Mat ref_fftData_R;
	Mat ref_fftData_I;
	ref_fftData_R = ref_Aline_R_I[0];
	ref_fftData_I = ref_Aline_R_I[1];

	// exponent operator is pow()
	int size = pow(2, 15) - ref_Aline.rows;

	// Do the zero padding
	copyMakeBorder(ref_fftData_R, ref_fftData_R, 0, size, 0, 0, BORDER_CONSTANT, 0);
	copyMakeBorder(ref_fftData_I, ref_fftData_I, 0, size, 0, 0, BORDER_CONSTANT, 0);
	//ref_fftData_pad = ref_fftData_R + (1j.*ref_fftData_I); // from MATLAB, you need to do this somehow.. maybe not?


	Mat tar_fftData_R;
	Mat tar_fftData_I;

	copyMakeBorder(ref_FFTData, tar_fftData_R, 0, size, 0, 0, BORDER_CONSTANT, 0);
	copyMakeBorder(ref_FFTData, tar_fftData_I, 0, size, 0, 0, BORDER_CONSTANT, 0);
	//tar_fftData_pad = tar_fftData_R + (1j.*tar_fftData_I); // from MATLAB, you also need to do this .. maybe not?

	// ifft() the zero padded 
	Mat refCalSig_raw;
	Mat tarCalSig_raw;

	dft(ref_fftData_R, refCalSig_raw, DFT_INVERSE | DFT_REAL_OUTPUT);
	dft(ref_fftData_I, tarCalSig_raw, DFT_INVERSE | DFT_REAL_OUTPUT);
	
	// fft() this for some reason ...
	Mat refCalSig;
	Mat tarCalSig;

	dft(refCalSig_raw, refCalSig, DFT_COMPLEX_OUTPUT);
	dft(tarCalSig_raw, tarCalSig, DFT_COMPLEX_OUTPUT);

	// cplxConjCalSig = tarCalSigs.*repmat(conj(refCalSig), [1 size(tarCalSigs, 2)]); - MATLAB
	Mat ones(Size(1, 32768), CV_32F, Scalar(1)); //initialize matrix of all ones
	Mat cplx[] = { Mat_<float>(ref_fftData_R), Mat::zeros(ref_fftData_R.size(), CV_32F) };
	Mat conjTarCal;

	merge(cplx, 2, conjTarCal);
	merge(cplx, 2, ones);

	mulSpectrums(ones, tarCalSig, conjTarCal, 0, true);

	Mat repTarCal(32768, 400, CV_32F);
	int jx = 0;
	for (jx = 0; jx < repTarCal.cols; jx++)
	{
		repTarCal.col(jx) = tarCalSig.col(jx);
	}


	return DiffPhase;
}

