#ifndef PRE_SCRIPT_H
#define PRE_SCRIPT_H
#include <iostream>
#include <vector>
#include "tinyxml2-master\tinyxml2-master\tinyxml2.h"
#include <opencv2\opencv.hpp>
#include "misc.h"

using namespace std;
using namespace cv;
using namespace tinyxml2;

// Read in the LUT and store it in a vector
vector<double> readLUT(string fn_ResParam);

// MJ's function
vector<double> estPhaseDiff(Mat ref_Aline, Mat ref_FFTData, int offsetIdx);

Mat hilbert(Mat rawData);


// parameters class, this will have number of BM scans, number of frames, and sizes
// use getParameters() to set the data in this class
// Consider placing the entire volume in a class ... object oriented design?
class volume_parameters
{
public:
	int width;
	int height;
	int Number_of_Frames;
	int Number_of_Volumes;
	
	int Number_of_BM_scans;
	int ImageSize;
};

// Reads data from .xml file into parameters object
void getParameters(volume_parameters &parameters, XMLDocument &doc);

// Passing data structures by reference is faster and recommended
Mat proCplxAOOCT(volume_parameters &parameters, const char *fn_unp, vector<double> &LUT);


#endif


