#include <iostream>
#include <string>
#include <iterator>
#include <functional>
#include <vector>
#include <algorithm>
#include <fstream>
#include "Pre_script.h"
#include "tinyxml2-master\tinyxml2-master\tinyxml2.h"
#include <opencv2\opencv.hpp>
#include "misc.h"

using namespace std;
using namespace tinyxml2;
using namespace cv;


int main()
{
	/* First we want to read in the LUT and possible set file directories. We also want to
	somehow implement getParameters() to get the necessary parameters from the .xml file */

	// Read in LUT to in stream 
	vector<double> rescaleParam;
	string fn_ResParam = "LUTSS.bin";
	ifstream fid_rescaleParam(fn_ResParam, ios::in | std::ios::binary);

	// This is the LUT
	rescaleParam = readLUT(fn_ResParam);

	/* Now we need to implement getParameters() on the .xml file */
	// http://leethomason.github.io/tinyxml2/
	// http://blog-ythu.github.io/2012/07/11/using-MSXML/
	//string fn_xml = "9_28_11-H932-OD-FOV.xml";
	const char* fn_xml = "9_28_11-H932-OD-FOV.xml";
	//const char* fn_xml = "19_08_18-RPE65_SuperAvg_M_2.xml";
	XMLDocument doc;
	volume_parameters parameters;

	// Load the xml file into the XMLDocument type
	doc.LoadFile(fn_xml);

	// Load parameters object with data from .xml document
	getParameters(parameters, doc);

	// Motion Correction //
	Mat ProcdData;
	const char *fn_unp = "9_28_11-H932-OD-FOV.unp"; // .unp Human AO data
	//const char *fn_unp = "19_08_18-RPE65_SuperAvg_M_2.prof"; // Ringo's .prof data

	ProcdData = proCplxAOOCT(parameters, fn_unp, rescaleParam);

	return 0;
}