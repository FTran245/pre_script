#include <iostream>
#include <string>
#include <iterator>
#include <functional>
#include <vector>
#include <algorithm>
#include <stdio.h>
#include <fstream>
#include "Pre_script.h"
#include "tinyxml2-master\tinyxml2-master\tinyxml2.h"
#include <opencv2\opencv.hpp>
#include "opencv2\imgproc\imgproc.hpp"
#include "opencv2\core\core.hpp"
#include "opencv2\highgui\highgui.hpp"

using namespace std;
using namespace cv;

// Consider passing by reference to improve run time
Mat fft1D(Mat rawData)
{
	// on the border zero pad
	Mat padded;
	int m = getOptimalDFTSize(rawData.rows);
	int n = getOptimalDFTSize(rawData.cols);

	copyMakeBorder(rawData, padded, m - rawData.rows, 0, n - rawData.cols, BORDER_CONSTANT, 0);

	Mat planes[] = { Mat_<float>(padded), Mat::zeros(padded.size(), CV_32F) };
	Mat complexI;
	merge(planes, 2, complexI); // Add to the expanded another plane with zeros make room for complex values

	// dft(src, dest, flags);
	dft(complexI, complexI, DFT_COMPLEX_OUTPUT); //complexI is the matrix we want I think...

	// planes[0] = Re(DFT(I), planes[1] = Im(DFT(I))
	split(complexI, planes);
	magnitude(planes[0], planes[1], planes[0]); // planes[0] = magnitude

	return complexI;
}

void checkDFT(Mat data)
{
	// CHECK
	Mat planes[] = { Mat_<float>(data), Mat::zeros(data.size(), CV_32F) };

	split(data, planes);
	magnitude(planes[0], planes[1], planes[0]); // planes[0] = magnitude

	Mat magI = planes[0];

	magI += Scalar::all(1); // switch to logarithmic scale
	log(magI, magI);

	// crop the spectrum, if it has an odd number of rows or columns
	magI = magI(Rect(0, 0, magI.cols & -2, magI.rows & -2));

	// rearrange the quadrants of Fourier image  so that the origin is at the image center
	int cx = magI.cols / 2;
	int cy = magI.rows / 2;

	Mat q0(magI, Rect(0, 0, cx, cy));   // Top-Left - Create a ROI per quadrant
	Mat q1(magI, Rect(cx, 0, cx, cy));  // Top-Right
	Mat q2(magI, Rect(0, cy, cx, cy));  // Bottom-Left
	Mat q3(magI, Rect(cx, cy, cx, cy)); // Bottom-Right

	Mat tmp;                           // swap quadrants (Top-Left with Bottom-Right)
	q0.copyTo(tmp);
	q3.copyTo(q0);
	tmp.copyTo(q3);

	q1.copyTo(tmp);                    // swap quadrant (Top-Right with Bottom-Left)
	q2.copyTo(q1);
	tmp.copyTo(q2);

	normalize(magI, magI, 0, 1, CV_MINMAX); // Transform the matrix with float values into a
	
	// Display
	imshow("spectrum magnitude", magI);
	waitKey();
}

// This actually works https://docs.opencv.org/2.4/doc/tutorials/core/discrete_fourier_transform/discrete_fourier_transform.html
// Tried it on Lena
void dftExample()
{
	Mat img;
	img = imread("Lenna.png", CV_LOAD_IMAGE_GRAYSCALE);

	Mat padded;
	int m = getOptimalDFTSize(img.rows);
	int n = getOptimalDFTSize(img.cols); // on the border add zero values

	copyMakeBorder(img, padded, m - img.rows, 0, n - img.cols, BORDER_CONSTANT, 0);

	Mat planes[] = { Mat_<float>(padded), Mat::zeros(padded.size(), CV_32F) };
	Mat complexI;
	merge(planes, 2, complexI);         // Add to the expanded another plane with zeros

	// Put this flag to use DFT on the rows
	// This will be good for what we're doing with the hilbert()
	dft(complexI, complexI, DFT_ROWS);

	split(complexI, planes);                   // planes[0] = Re(DFT(I), planes[1] = Im(DFT(I))
	magnitude(planes[0], planes[1], planes[0]);// planes[0] = magnitude
	Mat magI = planes[0];

	magI += Scalar::all(1);                    // switch to logarithmic scale
	log(magI, magI);

	// crop the spectrum, if it has an odd number of rows or columns
	magI = magI(Rect(0, 0, magI.cols & -2, magI.rows & -2));

	// rearrange the quadrants of Fourier image  so that the origin is at the image center
	int cx = magI.cols / 2;
	int cy = magI.rows / 2;

	Mat q0(magI, Rect(0, 0, cx, cy));   // Top-Left - Create a ROI per quadrant
	Mat q1(magI, Rect(cx, 0, cx, cy));  // Top-Right
	Mat q2(magI, Rect(0, cy, cx, cy));  // Bottom-Left
	Mat q3(magI, Rect(cx, cy, cx, cy)); // Bottom-Right

	Mat tmp;                           // swap quadrants (Top-Left with Bottom-Right)
	q0.copyTo(tmp);
	q3.copyTo(q0);
	tmp.copyTo(q3);

	q1.copyTo(tmp);                    // swap quadrant (Top-Right with Bottom-Left)
	q2.copyTo(q1);
	tmp.copyTo(q2);

	normalize(magI, magI, 0, 1, CV_MINMAX); // Transform the matrix with float values into a

	imshow("Input Image", img);    // Show the result
	imshow("spectrum magnitude", magI);
	waitKey();
}