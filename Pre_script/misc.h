#ifndef MISC_H
#define MISC_H
#include <iostream>
#include <vector>
#include "tinyxml2-master\tinyxml2-master\tinyxml2.h"
#include <opencv2\opencv.hpp>
#include "Pre_script.h"

using namespace std;
using namespace cv;

Mat fft1D(Mat rawData);

Mat initCplxMat(int rows, int cols);

void checkDFT(Mat data);

void dftExample();

#endif
